package com.nuwan.convertwebsite;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    WebView webView;
    RelativeLayout emptyLayout;
    ImageButton refreshButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refreshButton = findViewById(R.id.refreshButton);
        emptyLayout = findViewById(R.id.emptyLayout);
        webView = findViewById(R.id.webView);

        if (isNetworkAvailable()) {
            loadWebView();
        } else {
            emptyLayout.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);
        }

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    loadWebView();
                } else {
                    emptyLayout.setVisibility(View.VISIBLE);
                    webView.setVisibility(View.GONE);
                }
            }
        });

//        NetworkReciever networkReciever = new NetworkReciever();
//        String networkString = networkReciever.status;
//
//        Toast.makeText(this, networkString, Toast.LENGTH_SHORT).show();
    }

    private void loadWebView() {
        
        webView.setVisibility(View.VISIBLE);
        emptyLayout.setVisibility(View.GONE);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("https://slicg.app");
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            return networkInfo != null &&networkInfo.isConnected();
        }

        return false;
    }
}
