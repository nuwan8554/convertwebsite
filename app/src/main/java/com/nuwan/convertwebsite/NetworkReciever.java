package com.nuwan.convertwebsite;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NetworkReciever extends BroadcastReceiver {

    public static String status = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        status = NetworkState.getConnectivityStatusString(context);
    }
}
